# Assesment PHP Master
## Kurulum
- /app klasörü içinde yer alan schema.sql'in bir mysql veritabanına aktarılması gerekmetedir.
- artından .env dosyası oluşturulmalı içinde .env.example içinde yer alan değişkenler veritabanına uygun bir şekilde doldurulmalıdır.
- app içerisinde yer alan index.html içerisindeki http://localhost:8888/assessment-php-master/app/post.php ve http://localhost:8888/assessment-php-master/app/employee.php projeyi yayınladığınız urller ile değiştiştirilmedilir

## Proje dosyaları
### index.php
    - task.csv'yi içeri aktaran bir betik yer alır.
### post.php
    - index.html'in js ile .csv dosyalarını post ettiği php betiği yer alır.
### employee.php
    - employeeList'i dönen bir endpoint

## Proje Klasörleri
### Adaptor
    - CsvToArray.php: CsvToArray, gelen Csv dosyalarını bir Array'e dönüştüren Adaptor bir sınıftır.

### config
    -Config.php Composer'i yükleyen ve .dotenv'i initialize eden bir config dosyası

### database
    - Connection.php, Connection isminde veritabanına bağlanma object oluşturan bir sınıftır.

### Form
    - EmployeeForm, Employee oluşturmadan önce Employee için verilerin doldurulması-değiştirilmesi ve Validasyon işlemi yapılması için oluşturulmuş bir Form sınıfıdır.
    - Sınıf, validator sınıfları kullanarak validasyon işlemlerini sağlar.
    - sınıf validationRules array'ine yeni validatorler ekleyerek yeni validasyon kuralları oluşturulabilecek bir yapıya sahiptir.
    - ```        $this->validationRules = [
            'name' => 'App\Validator\NotEmptyValidator',
            'surname' => 'App\Validator\NotEmptyValidator',
            'point'=> 'App\Validator\NotEmptyValidator',
            'email' => 'App\Validator\EmailValidator',
            'phone' => 'App\Validator\PhoneValidator', 
        ]; ```
### Model
- EmployeeModel.php, Employee Model, Employee Objeleri için oluşturulmuş çeşitli getter ve setter ve uygun array output formatını oluşturmak için çeşitli methodlara sahiptir.

### Repository
-   EmployeeRepository: Employeelerin tümünü çekmek tek 1 tanesini çekmek veya employee veritabanına kayıt etmek için bir repository sınıfıdır.

### Validator
- Email, NotEmpy ve Phone Validator olmak üzere 3 ayrı validator, ->validate() methodu ile verilmiş değerin validasyon kuralına uygun olup olmadığını döner.

