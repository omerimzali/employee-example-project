<?php
namespace App\Adaptor;

class CsvtoArray{

    function __construct(string $fileName) {

        $this->reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
        $this->file = $this->reader->load($fileName);
        $this->worksheet = $this->file->getActiveSheet();
        $this->rows = $this->worksheet->toArray();
        array_shift($this->rows);
        
      }

      public function getArray():Array{
        return $this->rows;
      }
}