<?php

namespace App\Repository;
use App\Model\EmployeeModel as Employee;
use App\Form\EmployeeForm;


class EmployeeRepository
{

    function __construct($conn)
    {
        $this->connection = $conn->connection;


    }

    public function create(Employee $employee){

        $employeeForm = new EmployeeForm($employee);
     
        if($employeeForm->validate()){
            $employe_insert = $this->insert($employee);
            return ["message" =>   $employe_insert["message"], "error" =>  $employe_insert["error"]];
        }else{
            return["message" =>  "Validation Fail", "error" => true, "validation_error"=>true];
        }

    }

    public function insert(Employee $employee){

        /** Create Employee here */
        $sql = "INSERT INTO Employee (name, surname, email,phone,employee_id, point) VALUES (?,?,?,?,?,?)";
        try{
        $insert = $this->connection->prepare($sql);
        $insert->execute($employee->getArray());
        return ["message" => "Employee Created", "error" => false];
        }catch (\PDOException $e)  {
       
            if(($e->errorInfo[0] == 23000)) {
                return ["message"=>"Duplicated Data", "error"=> true];
            }else{
                return ["message"=>$e->getMessage(), "error"=> true];
            }
        } 
        
    }

    public function getAll()
    {
        $query = $this->connection->prepare("SELECT * FROM Employee");
        $query->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        $employeeList= [];

        foreach($result as $employeeData){
            $employeeList[] = new Employee($employeeData);
        }
        return $employeeList;
    }

    public function find(int $id){
        $query = $this->connection->prepare("SELECT * FROM Employee WHERE id=?");
        $query->execute([$id]);
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if($result){
            return  new Employee($result);
        }
    }

}
