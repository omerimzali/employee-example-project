<?php

namespace App\Database;
use stdClass;


class Connection
{
    function __construct($servername, $dbname, $username, $password)
    {
        $this->connection = new stdClass();
        $this->servername = $servername;
        $this->dbname =  $dbname;
        $this->username = $username;
        $this->password = $password;
    }




    public function connect()
    {

        try {
            $this->connection = new \PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
}
