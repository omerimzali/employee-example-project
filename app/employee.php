<?php


require "initialize.php";
use App\Repository\EmployeeRepository;
use App\Adaptor\CsvtoArray;
use App\Model\EmployeeModel;
use App\Form\EmployeeForm;
use App\Database\Connection;
use App\Validator\NotEmptyValidator;
$adaptor = new CsvtoArray('task.csv');
$employeeArray = $adaptor->getArray();
$validator = new NotEmptyValidator("a");
$connection = new Connection($_ENV['DB_HOST'], $_ENV['DB_NAME'],$_ENV['DB_USER'],$_ENV['DB_PASSWORD']);
$connection->connect();
$employeeRepository = new EmployeeRepository($connection);

$employeeList = $employeeRepository->getAll();
print_r(json_encode($employeeList));