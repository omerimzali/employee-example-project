<?php
namespace App\Validator;

class NotEmptyValidator
{
    function __construct(string $string)
    {
        $this->string = $string;
    }

    function validate()
    {
        if (empty($this->string)) {
            return false;
        } else {
            return true;
        }
    }
}
