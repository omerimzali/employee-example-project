<?php

namespace App\Validator;

class PhoneValidator
{
    function __construct($phone)
    {
        $this->phone = $phone;
    }

    function validate()
    {
        if (strlen(((string)$this->phone)) == 10 && is_numeric($this->phone)) {
            return true;
        } else {
            return false;
        }
    }
}
