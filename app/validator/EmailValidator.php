<?php

namespace App\Validator;

class EmailValidator
{
    function __construct(string $email)
    {
        $this->email = $email;
    }

    function validate()
    {
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return false;
        } else {
            return true;
        }
    }
}
