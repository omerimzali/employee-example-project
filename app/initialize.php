<?php

require("config/config.php");
require("adaptor/CsvToArray.php");
require("database/connection.php");
require("form/EmployeeForm.php");
require("repository/EmployeeRepository.php");
require("validator/EmailValidator.php");
require("validator/NotEmptyValidator.php");
require("validator/PhoneValidator.php");

require("model/EmployeeModel.php");

