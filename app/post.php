<?php


require "initialize.php";
use App\Repository\EmployeeRepository;
use App\Adaptor\CsvtoArray;
use App\Model\EmployeeModel;
use App\Form\EmployeeForm;
use App\Database\Connection;
use App\Validator\NotEmptyValidator;
$files = $_FILES;

$adaptor = new CsvtoArray($files["data"]["tmp_name"]);
$employeeArray = $adaptor->getArray();
$connection = new Connection($_ENV['DB_HOST'], $_ENV['DB_NAME'],$_ENV['DB_USER'],$_ENV['DB_PASSWORD']);
$connection->connect();




$employeeRepository = new EmployeeRepository($connection);

$head = ["name","surname","email","employeeId","phone","point"];
$results = [];
foreach($employeeArray as $employee){

    $employee = new EmployeeModel(array_combine($head,$employee));
  
    $results[] = $employeeRepository->create($employee);
    
}
$error= 0;
$validation_error=0;
foreach($results as $result){
    if($result["error"]){
        $error = $error+1;
    }
    if($result["validation_error"]){
        $validation_error=$validation_error+1;
    }
}

print_r(json_encode(
    ["message"=>
    sprintf("Toplam %s satırı eklerken hata oluştur. Bunlardan %s tanesi telefon numarası ya da email adresinin validasyon problemi sebebiyle geri kalanları Unique olması gereken değerlerin Unique olmamamsı sebeibyle oluştu.",
    $error,$validation_error)
]
));