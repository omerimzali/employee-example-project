<?php
namespace App\Model;

use JetBrains\PhpStorm\ArrayShape;

class EmployeeModel {

    function __construct(Array $data) {
      $this->rawData = $data;
      $this->name = $data["name"];
      $this->surname = $data["surname"];
      $this->email = $data["email"];
      $this->employeeId = $data["employeeId"];
      $this->phone = $data["phone"];
      $this->point = $data["point"];

    }

    function getRawData():Array 
    {
      return $this->rawData;
    }

    function getArray():Array
    {
      return [$this->name, $this->surname, $this->email, $this->phone,$this->employeeId,  $this->point];
    }

    function getName(): string{
    
      return $this->name;
    } 

    function getSurname(): string{
        return $this->surname;
    }

    function getEmail(): string{
        return $this->email;
    }


    function getPhone(): string{
        return $this->phone;
    }

    function setPhone(int $phone){
      $this->phone = $phone;
      return $this;
    }
    public function getEmployeeId():int{
        return $this->employeeId;
    }

    public function getPoint():int{
        return $this->point;
    }

    public function getFullName():string{
        return $this->name." ".$this->surname;
    }
    
    
  
  }