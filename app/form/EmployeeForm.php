<?php
namespace App\Form;

use App\Model\EmployeeModel;
use App\Validator\EmailValidator;
use App\Validator\PhoneValidator;
use App\Validator\NotEmptyValidator;
class EmployeeForm
{
    function __construct(EmployeeModel $employee)
    {
        $this->employee = $employee;
        /** validation rules are prioritized here */
        $this->validationRules = [
            'name' => 'App\Validator\NotEmptyValidator',
            'surname' => 'App\Validator\NotEmptyValidator',
            'point'=> 'App\Validator\NotEmptyValidator',
            'email' => 'App\Validator\EmailValidator',
            'phone' => 'App\Validator\PhoneValidator', 
        ];

        $this->fillPhone($this->employee);
       
    }

    function fillPhone(EmployeeModel $employee){
        $phone = (int)str_replace(['+','-','.'],'', $employee->phone);
        $employee->setPhone($phone);
    }

    function validate(){
        $validate = true;
        foreach($this->validationRules as $validationKey => $validator){
            
            $validator = new $validator($this->employee->$validationKey);
            if(!$validator->validate()){
                $validate=false;
                break;
            }
        }
        return $validate;

    }
}
